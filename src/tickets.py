
from datetime import date, datetime, timedelta
import uuid
import random
from collections import namedtuple


today = datetime.today()
print(today)
start = datetime(today.year, today.month, today.day)
monday_start = start - timedelta(days=today.weekday())
print(today, start, monday_start)

Route = namedtuple('Route', "idx, citi_tu, date, max_seat")

ROUTES = [
    Route(0, 'Odessa-Kyiv', monday_start + timedelta(days=0, hours=8, minutes=30), 55),
    Route(1, 'Odessa-Kyiv', monday_start + timedelta(days=2, hours=10, minutes=30), 50),
    Route(2, 'Odessa-Kyiv', monday_start + timedelta(days=4, hours=12, minutes=45), 55),
    Route(3, 'Poltava-Kyiv', monday_start + timedelta(days=1, hours=14, minutes=20), 45),
    Route(4, 'Poltava-Kyiv', monday_start + timedelta(days=3, hours=16, minutes=10), 50),
    Route(5, 'Poltava-Kyiv', monday_start + timedelta(days=5, hours=18, minutes=20), 45),
    Route(6, 'Kharkov-Kyiv', monday_start + timedelta(days=0, hours=7, minutes=50), 55),
    Route(7, 'Kharkov-Kyiv', monday_start + timedelta(days=3, hours=9, minutes=30), 50),
    Route(8, 'Kharkov-Kyiv', monday_start + timedelta(days=6, hours=11, minutes=20), 55),
    Route(9, 'Zhytomyr-Kyiv', monday_start + timedelta(days=1, hours=17, minutes=10), 30),
    Route(10, 'Zhytomyr-Kyiv', monday_start + timedelta(days=3, hours=18, minutes=20), 25),
    Route(11, 'Zhytomyr-Kyiv', monday_start + timedelta(days=5, hours=20,minutes=30), 35)
]

# ticket: idx, route_id, seat_number, control_code
tickets = [
    (0, 1, 12, uuid.uuid4()),
    (1, 1, 14, uuid.uuid4()),
    (2, 1, 15, uuid.uuid4()),
]


menu = """
Меню
0 - Выход
1 - Расписание рейсов
2 - Выбрать маршрут
3 -

"""

if __name__ == '__main__':

    while True:
        print(menu)
        choice = input("Выберите действие из МЕНЮ: ")
        match choice:
            case "0":
                break
            case "1":
                print('_idx_\t_citi_tu_\t    ___date___\t_time_\t_max_seat_')
                for item in ROUTES:
                    print(f'{item[0]:4}\t{item[1]:15}\t{item[2]}\t{item[3]:6}\t')

            case "2":
                city = input('Выберите маршрут: ')
                for item in ROUTES:
                    if city in item.citi_tu:
                     print(item)

                route_id = int(input('Выберите номер рейса: '))
                if ROUTES[route_id][2] <= datetime.now():
                    print('К сожалению этот рейс уже отправился')
                    continue
                max_seat = ROUTES[route_id][3]
                seats = list(range(1, max_seat + 1))
                seats_sold = []
                for ticket_item in tickets:
                    if ticket_item[1] == route_id:
                        seats_sold.append(ticket_item[2])
                seats_tu_sell = []
                for seat_item in seats:
                    if seat_item not in seats_sold:
                        seats_tu_sell.append(seat_item)
                print("Свободные места:" )
                print(seats_tu_sell)

                seat = int(input('Выберите место:  '))
                #seat =  random.choice(seats_tu_sell)
                if seat in seats_tu_sell:
                    tickets.append((len(tickets), route_id, seat, uuid.uuid4()))
                    print(f'({tickets[-1][0]}, {route_id}, {seat}, {tickets[-1][3]})')
                else:
                    print("Это место продано")