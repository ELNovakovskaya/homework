
a = 2*3
print(a)
print(type(a))

print((3*3+8)/3, type((3*3+8)/3))


b = 8//3
print(8//3, type(8//3))

print(8%3, type(8%3))

print(5**2, type(5**2))

print('Hello '+'world', type('Hello '+'world'))

s1 = "My name is Helen"
print(s1)
s2 = 'Hello world'
print(s2)

print(s1[:11])
print(s1[2:12])
print(s1[-10:])
print(s1[::-1])
print(s1[::2])
print(s1[1::2])

x = input("Введите число: ")
y = input("Введите число: ")
print(int(x)+int(y))
